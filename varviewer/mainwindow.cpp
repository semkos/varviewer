#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDesktopWidget>
#include <algorithm>
#include <QDebug>
#include <QMessageBox>
#include <QFile>
#include <QSplitter>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    centralWidget()->setLayout(ui->grid);
    this->move(QApplication::desktop()->screen()->rect().center() - this->rect().center());
    ui->tabTrackedVars->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tabMain->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tabMain->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    openocd = new OpenOCD;
    connect(openocd, SIGNAL(connected()), this, SLOT(openocdConnected()));
    connect(openocd, SIGNAL(disconnected()), this, SLOT(openocdDisconnected()));

    importdialog = new ImportDialog(this);
    connect(importdialog, SIGNAL(addVariable(Variable)), this, SLOT(importVariable(Variable)));

    QSplitter *splitter = new QSplitter;

    barchart = new BarChart;
    barchart->setYAxisRange(0,1000);
    connect(barchart, SIGNAL(axisAutoScaleChanged(bool)), this, SLOT(autoScaleChanged(bool)));

    splitter->addWidget(barchart);
    splitter->addWidget(ui->tabMain);
    splitter->setOrientation(Qt::Vertical);
    splitter->setChildrenCollapsible(0);
    splitter->setStretchFactor(0,1);
    splitter->setStretchFactor(1,0);
    ui->grid->addWidget(splitter, 0, 3, 6, 1);
    splitter->setStyleSheet("QSplitter::handle{height: 10px;}");

    ui->edYMax->setValidator(new QIntValidator(0, 99999999, this));
    ui->edYMin->setValidator(new QIntValidator(-99999999, 0, this));

    rtimer = new QTimer;
    connect(rtimer, SIGNAL(timeout()), this, SLOT(refreshGraphs()));
    rtimer->start(30);
}

MainWindow::~MainWindow()
{
    openocd->stopTracking();
    delete ui;
}

void MainWindow::refreshGraphs() {
    refreshMainTab();
    barchart->setValues(varsTracked.getValuesVector());
}

void MainWindow::on_actionImport_variables_triggered()
{
    importdialog->show();
}

void MainWindow::importVariable(const Variable &var) {
    // check if var is not imported yet
    if(varsTracked.findByName(var.name())!=-1) return;

    varsTracked.addVariable(var);
    barchart->addBar(var.name());

    loadTrackedVarsToTable();
    loadMainTab();
}

void MainWindow::loadTrackedVarsToTable() {
    int row;
    QTableWidgetItem *newItem;

    ui->tabTrackedVars->setRowCount(0);

    for(auto const &i: varsTracked.getVariablesVector()) {
        row = ui->tabTrackedVars->rowCount();
        ui->tabTrackedVars->setRowCount(row+1);
        newItem = new QTableWidgetItem(i.name());
        ui->tabTrackedVars->setItem(row, 0, newItem);
        newItem = new QTableWidgetItem(i.type());
        ui->tabTrackedVars->setItem(row, 1, newItem);
        newItem = new QTableWidgetItem(i.address());
        ui->tabTrackedVars->setItem(row, 2, newItem);
    }
}

void MainWindow::openocdConnected() {
    ui->butStart->setText("STOP");
    ui->butStart->setStyleSheet("background-color: red;");
    ui->statusBar->showMessage("Connected", 3000);
}

void MainWindow::openocdDisconnected() {
    ui->butStart->setText("Start");
    ui->butStart->setStyleSheet("background-color: green;");
    ui->statusBar->showMessage("Disconnected", 3000);
}

void MainWindow::on_butStart_clicked()
{
    if(openocd->isConnected()) {
        openocd->stopTracking();
    }
    else {
        openocd->setVariableVector(&varsTracked);
        openocd->setElfFile(importdialog->fileName());
        if(!openocd->startTracking()) {
            qDebug() << "Start Error";
            return;
        }
    }
}

void MainWindow::loadMainTab() {
    QTableWidgetItem *newItem;
    int row;

    ui->tabMain->setRowCount(0);

    for(auto const &i: varsTracked.getVariablesVector()) {
        row = ui->tabMain->rowCount();
        ui->tabMain->setRowCount(row+1);
        newItem = new QTableWidgetItem(i.name());
        ui->tabMain->setItem(row, 0, newItem);
        newItem = new QTableWidgetItem(i.type());
        ui->tabMain->setItem(row, 1, newItem);
        newItem = new QTableWidgetItem(i.address());
        ui->tabMain->setItem(row, 2, newItem);
        newItem = new QTableWidgetItem( tr("0x%1").arg(i.hexValue()) );
        ui->tabMain->setItem(row, 3, newItem);
        newItem = new QTableWidgetItem(QString::number(i.value()));
        ui->tabMain->setItem(row, 4, newItem);
    }
    barchart->setValues(varsTracked.getValuesVector());
}

void MainWindow::refreshMainTab() {
    int row=0;

    //for(auto const &i: varsTracked) {
    for(int l=0; l<varsTracked.size(); l++) {
        Variable i = varsTracked.at(l);
        ui->tabMain->item(row,3)->setText(tr("0x%1").arg(i.hexValue()));
        ui->tabMain->item(row,4)->setText(QString::number(i.value()));
        row++;
    }
}

void MainWindow::on_edYMax_editingFinished()
{
    barchart->setYAxisRange(ui->edYMin->text().toDouble(), ui->edYMax->text().toDouble());
}

void MainWindow::on_edYMin_editingFinished()
{
    barchart->setYAxisRange(ui->edYMin->text().toDouble(), ui->edYMax->text().toDouble());
}

void MainWindow::on_butYMaxAuto_clicked()
{
    barchart->setYAxisAutoScale(!barchart->axisAutoScale(QwtPlot::yLeft));
}

void MainWindow::autoScaleChanged(const bool &state) {
    if(state==1) {
        ui->butYMaxAuto->setStyleSheet("background-color: blue;");
    }
    else {
        ui->butYMaxAuto->setStyleSheet("");
    }
}
