#include "barchart.h"
#include <qwt_plot_renderer.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_barchart.h>
#include <qwt_column_symbol.h>
#include <qwt_plot_layout.h>
#include <qwt_legend.h>
#include <qwt_scale_draw.h>
#include <qwt_plot_grid.h>

/*!
 * \brief Widok osi wykresu słupkowego
 */
class DistroScaleDraw: public QwtScaleDraw
{
public:
    DistroScaleDraw( Qt::Orientation orientation, const QStringList &labels ):
        d_labels( labels )
    {
        setTickLength( QwtScaleDiv::MinorTick, 0 );
        setTickLength( QwtScaleDiv::MediumTick, 0 );
        setTickLength( QwtScaleDiv::MajorTick, 2 );

        enableComponent( QwtScaleDraw::Backbone, false );

        if ( orientation == Qt::Vertical )
        {
            setLabelRotation( 60.0 );
        }
        else
        {
            setLabelRotation( -20.0 );
        }

        setLabelAlignment( Qt::AlignRight | Qt::AlignVCenter );
    }

    virtual QwtText label( double value ) const
    {
        QwtText lbl;

        const int index = qRound( value );
        if ( index >= 0 && index < d_labels.size() )
        {
            lbl = d_labels[ index ];
        }
            
        return lbl;
    }

private:
    const QStringList d_labels;
};

/*!
 * \brief Widok słupków wykresu słupkowego
 */
class DistroChartItem: public QwtPlotBarChart
{
public:
    DistroChartItem():
        QwtPlotBarChart( "Page Hits" )
    {
        setLegendMode( QwtPlotBarChart::LegendBarTitles );
        setLegendIconSize( QSize( 10, 14 ) );
        setLayoutPolicy( AutoAdjustSamples );
        setLayoutHint( 4.0 ); // minimum width for a single bar

        setSpacing( 30 ); // spacing between bars
    }

    void addDistro( const QString &distro, const QColor &color )
    {
        d_colors += color;
        d_distros += distro;
        itemChanged();
    }

    virtual QwtColumnSymbol *specialSymbol(
        int index, const QPointF& ) const
    {
        // we want to have individual colors for each bar
        QwtColumnSymbol *symbol = new QwtColumnSymbol( QwtColumnSymbol::Box );
        symbol->setLineWidth( 1 );
        symbol->setFrameStyle( QwtColumnSymbol::Plain );

        QColor c( Qt::white );
        if ( index >= 0 && index < d_colors.size() )
            c = d_colors[ index ];

        symbol->setPalette( c );
    
        return symbol;
    }

    virtual QwtText barTitle( int sampleIndex ) const
    {
        QwtText title;
        if ( sampleIndex >= 0 && sampleIndex < d_distros.size() )
            title = d_distros[ sampleIndex ];

        return title;
    }

private:
    QList<QColor> d_colors;
    QList<QString> d_distros;
};

/*********************************** BARCHART FUNCTIONS *************************************/

BarChart::BarChart( QWidget *parent ):
    QwtPlot( parent )
{

    setAutoFillBackground( true );

    QwtPlotCanvas *canvas = new QwtPlotCanvas();
    canvas->setLineWidth( 1 );
    canvas->setFrameStyle( QFrame::Box );

    QPalette canvasPalette( QColor( "grey" ) );
    canvasPalette.setColor( QPalette::Foreground, QColor( "Black" ) );
    canvas->setPalette( canvasPalette );

    setCanvas( canvas );

    d_barChartItem = new DistroChartItem();

    d_barChartItem->setSamples( samples );

    d_barChartItem->attach( this );

    // grid
    QwtPlotGrid *grid = new QwtPlotGrid;
    grid->enableXMin( true );
    grid->setMajorPen( Qt::white, 0, Qt::DotLine );
    grid->setMinorPen( Qt::gray, 0 , Qt::DotLine );
    grid->enableYMin(1);
    grid->enableXMin(0);
    grid->attach( this );

    setOrientation( 0 );
    setAutoReplot( true );
}

void BarChart::addBar(const QString name) {
    d_distros += name;
    samples += 100;
    d_barChartItem->addDistro(name, QColor(qrand()%255,qrand()%255,qrand()%255));
    d_barChartItem->setSamples(samples);
    //setValues(samples);
    setOrientation(!orientation);
}

void BarChart::setValues(const QVector<double> &values) {
    d_barChartItem->setSamples(values);
}

void BarChart::setYAxisRange(const double &lower, const double &upper) {
    setAxisScale(QwtPlot::yLeft, lower, upper, 0);
    emit axisAutoScaleChanged(0);
}

void BarChart::setYAxisAutoScale(const bool &state) {
    setAxisAutoScale(QwtPlot::yLeft,state);
    emit axisAutoScaleChanged(state);
}

void BarChart::setOrientation( int o )
{
    orientation = ( o == 0 ) ? Qt::Vertical : Qt::Horizontal;

    int axis1 = QwtPlot::xBottom;
    int axis2 = QwtPlot::yLeft;

    if ( orientation == Qt::Horizontal )
        qSwap( axis1, axis2 );

    d_barChartItem->setOrientation( orientation );

    // x axis scale
    if(d_distros.size() == 1)
        setAxisScale(axis1, 0, 1, 1);
    else
        setAxisScale(axis1, 0, d_distros.size()-1, 1);

    setAxisScaleDraw( axis1, new DistroScaleDraw( orientation, d_distros ) );
    setAxisMaxMinor( axis2, 3 );

    QwtScaleDraw *scaleDraw = new QwtScaleDraw();
    scaleDraw->setTickLength( QwtScaleDiv::MediumTick, 4 );
    setAxisScaleDraw( axis2, scaleDraw );

    plotLayout()->setCanvasMargin( 0 );
    replot();
}

void BarChart::exportChart()
{
    QwtPlotRenderer renderer;
    renderer.exportTo( this, "distrowatch.pdf" );
}
