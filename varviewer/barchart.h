#ifndef _BAR_CHART_H_
#define _BAR_CHART_H_

/*!
 * \file
 * \brief Definicja klasy BarChart
 *
 * Definicja klasy wykresu słupkowego, opartej na QwtPlot
 */

#include <qwt_plot.h>
#include <qstringlist.h>

class DistroChartItem;

/*!
 * \brief Wykres słupkowy
 *
 * klasa realizuje graficzną reprezentację wartości zmiennych w postaci
 * wykresu słupkowego.
 */
class BarChart: public QwtPlot
{
    Q_OBJECT

public:
    BarChart( QWidget * = NULL );
    void addBar(const QString name);
    void setValues(const QVector<double> &values);
    void setYAxisRange(const double &lower, const double &upper);
    void setYAxisAutoScale(const bool &state);

public Q_SLOTS:
    void setOrientation( int );
    void exportChart();

signals:
    void axisAutoScaleChanged(const bool &state);

private:
    void populate();

    DistroChartItem *d_barChartItem;
    QStringList d_distros;
    QVector< double > samples;
    Qt::Orientation orientation;
};

#endif
