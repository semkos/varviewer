#ifndef VARIABLE_H
#define VARIABLE_H

/*!
 * \file
 * \brief Definicja klasy Variable oraz VariableVector
 *
 * Definicje klas reprezentujących zmienne
 * znajdujące się w pamięci RAM mikrokontrolera.
 */

#include <QString>
#include <QVector>
#include <QMutex>

/*!
 * \brief Reprezentuje zmienne w pamięci mikrokontrolera
 *
 * Klasa reprezentuje zmienne znajdujące się w pamięci RAM mikrokontrolera.
 */
class Variable
{
public:
    /*!
     * \brief Konstruktor bezparametryczny
     */
    Variable();
    /*!
     * \brief Inicjalizuje podstawowe dane o zmiennej
     * \param[in] name - nazwa zmiennej
     * \param[in] type - typ zmiennej
     * \param[in] addr - adres zmiennej w pamięci mikrokontrolera
     */
    Variable(const QString &name, const QString &type, const QString &addr);
    /*!
     * \brief Nazwa zmiennej
     * \return Zwraca nazwę zmiennej
     */
    QString name() const { return _name; }
    /*!
     * \brief Typ zmiennej
     * \return Zwraca typ zmiennej
     */
    QString type() const { return _type; }
    /*!
     * \brief Adres zmiennej
     * \return Zwraca adres zmiennej w pamięci mikrokontrolera
     */
    QString address() const { return _address; }
    /*!
     * \brief Wartość zmiennej w systemie szesnastkowym
     * \return Zwraca stringa z wartością zmiennej w systemie szesnastkowym
     */
    QString hexValue() const { return _raw_value; }
    /*!
     * \brief Wartość zmiennej
     * \return Zwraca wartość zmiennej
     */
    double value() const { return _value; }
    /*!
     * \brief Sprawdza czy wartośc może przyjmować wartośći ujemne
     * \return 1 - jeśli zmienna może przyjać wartości ujemne <br>
     * 0 - jeśli nie może
     */
    bool isSigned() const { return _signed; }
    /*!
     * \brief Ustawia nazwę zmiennej
     * \param name - nazwa zmiennej
     */
    void setName(const QString &name) { _name = name; }
    /*!
     * \brief Ustawia typ zmiennej
     * \param type - typ zmiennej
     */
    void setType(const QString &type);
    /*!
     * \brief Ustawia adres zmiennej
     * \param addr - adres zmiennej w pamięci mikrokontrolera
     */
    void setAddress(const QString &addr);
    /*!
     * \brief Ustawia wartość
     * \param value - String z wartością zmiennej w systemie szesnastkowym
     */
    void setValue(const QString &value);

    /*!
     * \brief Rozmiar zmiennej w pamięci
     *
     * Wartość określa ile bajtów w pamięci mikrokontrolera zajmuje dana zmienna.
     */
    enum ByteLength { CHAR=1, SHORT=2, LONG=4 };

private:
    QString _name, _type, _address, _raw_value;
    unsigned int _raddr;
    double _value;
    bool _signed;
};


/*!
 * \brief Wektor obiektów typu Variable
 *
 * Wektor obiektów typu Variable. Klasa zapewnia wygodny interfejs
 * dostępu do wykorzystywanych danych oraz zabezpiecza dostęp z wielu
 * wątków jednocześnie.
 */
class VariableVector : private QVector<Variable>
{
public:
    Variable at(int i);
    using QVector<Variable>::size;
    void addVariable(const Variable &var);
    int findByName(const QString &name);
    bool setValueByAddress(const QString &value, const QString &addr);
    QVector<double> getValuesVector();
    QVector<Variable> getVariablesVector();

private:
    QMutex mutex;
};

#endif // VARIABLE_H
