#include "importdialog.h"
#include "ui_importdialog.h"
#include <QDesktopWidget>
#include <QFileDialog>
#include <QDebug>
#include <QProcess>
#include <QTreeWidgetItem>
#include <QStringList>
#include <QRegularExpression>

ImportDialog::ImportDialog(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ImportDialog)
{
    ui->setupUi(this);
    centralWidget()->setLayout(ui->grid);
    this->move(QApplication::desktop()->screen()->rect().center() - this->rect().center());

    gdb.setProcessChannelMode(QProcess::MergedChannels);

    ui->tabVariables->setSelectionMode(QAbstractItemView::ExtendedSelection);
}

ImportDialog::~ImportDialog()
{
    delete ui;
}

void ImportDialog::on_butSelectFile_clicked()
{
    char recv[256];
    QString srecv;
    int li;
    QStringList rlist;
    Variable var;
    QList<QTreeWidgetItem *> items;

    QString newFileName = QFileDialog::getOpenFileName(this,
        tr("Select ELF File"), "/home/daniel/programy/stm32/f3_test/f3_test/TrueSTUDIO/f3_test/Debug", tr("ELF Files (*.elf)"));

    if(newFileName.size()<1) return;

    setFileName(newFileName);
    ui->editFile->setText(fileName());

    ui->tabVariables->clear();

    gdb.start("gdb", QStringList() << "-ex" << "info variables" << "-ex" << "Quit" << fileName());

    if (!gdb.waitForFinished())
        return;

    while(gdb.canReadLine()) {
        gdb.readLine(recv, 256);
        srecv = QString(recv);

        li = srecv.size()-2;
        if(li>1 && srecv[li]==';') {
            srecv.chop(2);
            rlist = srecv.split(' ');
            if(!rlist.contains("const")) {
                items.append(new QTreeWidgetItem((QTreeWidget*)0, QStringList() << rlist.last() << rlist[rlist.size()-2]));
            }
            //qDebug() << srecv;
        }
    }

    ui->tabVariables->insertTopLevelItems(0, items);
    ui->tabVariables->resizeColumnToContents(0);
    qDebug() << "Variables found:" << ui->tabVariables->topLevelItemCount();
    findVarsAddresses();
}

void ImportDialog::findVarsAddresses() {
    QString addr, name;
    QStringList ptype;

    gdb.start("gdb", QStringList() << fileName());
    gdb.waitForReadyRead(10000);    //----
    gdb.readAll();                  // |
    gdb.waitForReadyRead(10000);    // |
    gdb.waitForReadyRead(10000);    // |
    gdb.readAll();                  // |
    gdb.waitForReadyRead(10000);    // |
    gdb.readAll();                  //----> to ma tak być, żeby działało

    QTreeWidgetItemIterator it(ui->tabVariables);

    while(*it) {
        completeVarInfo(it);
        ++it;
    }

    gdb.close();
}

int ImportDialog::completeVarInfo(QTreeWidgetItemIterator &it) {
    QString name = (*it)->text(0);
    QStringList ptype = getVarType(name);

    // array
    if(name[name.size()-1]==']') {
        insertArray(name, it);
    }
    // struct
    else if(ptype[0].contains("struct")) {
        insertStruct(name, ptype, it);
    }
    // regular variable
    else {
        QString addr = getVarAddress(name);
        if(addr.size()>2) {
            (*it)->setText(2, addr);
        }
    }

    return 1;
}

QString ImportDialog::getVarAddress(const QString &name) {
    QString srecv;
    QStringList rsplitted;

    if(!gdb.isOpen()) return "";

    QByteArray dupa = tr("p &%1\n").arg(name).toUtf8();
    gdb.write(dupa);
    //qDebug() << name;
    gdb.waitForBytesWritten(1000);

    while(!gdb.canReadLine())
        gdb.waitForReadyRead(1000);

    while(gdb.canReadLine()) {
        QByteArray rr = gdb.readAll();
        srecv = QString(rr);
        rsplitted = srecv.split(' ');
        //qDebug() << srecv;
    }
    for(int k=0; k<rsplitted.size(); k++) {
        if(rsplitted[k][0]=='0' && rsplitted[k][1]=='x') {
            QString addr = rsplitted[k];
            int occ = addr.indexOf('\n');
            if(occ!=-1) addr.chop(addr.size()-occ);
            return addr;
        }
    }

    return "";
}

QStringList ImportDialog::getVarType(const QString &name) {
    QStringList list;

    if(!gdb.isOpen()) return QStringList();

    QByteArray cmd = tr("ptype %1\n").arg(name).toUtf8();
    gdb.write(cmd);
    while(!gdb.canReadLine())
        gdb.waitForReadyRead(100);

    while(gdb.canReadLine()) {
        QByteArray rr = gdb.readLine();
        rr.chop(1);
        if(rr[rr.size()-1]==';') rr.chop(1);
        list += rr;
    }

    return list;
}

int ImportDialog::insertArray(const QString &arname, QTreeWidgetItemIterator &pos) {
    QRegularExpression re("(?<name>.+)\\[(?<size>\\d+)\\]");
    QRegularExpressionMatch match = re.match(arname);
    if (!match.hasMatch()) return 0;

    QString name = match.captured("name");
    int arsize = match.captured("size").toInt();
    QString type = (*pos)->text(1);
    QString namewnr;

    for(int i=0; i<arsize; i++) {
        namewnr = tr("%1[%2]").arg(name).arg(i);
        QString addr = getVarAddress(namewnr);
        new QTreeWidgetItem(*pos, QStringList() << namewnr << type << addr);
    }
    pos+=arsize;

    return 1;
}

int ImportDialog::insertStruct(const QString &name, const QStringList &list, QTreeWidgetItemIterator &pos) {
    QStringList tmplist;
    QString rfullname, addr;
    QTreeWidgetItemIterator lpos = pos;

    for(int i=1; i<list.size()-1; i++) {
        tmplist = list[i].split(' ');
        rfullname = tr("%1.%2").arg(name).arg(tmplist.last());
        addr = getVarAddress(rfullname);
        if(addr>2) {
            new QTreeWidgetItem(*pos, QStringList() << rfullname << tmplist[tmplist.size()-2]);
            lpos++;
            completeVarInfo(lpos);
        }
    }
    pos=lpos;

    return 1;
}

void ImportDialog::on_butImport_clicked()
{
    for(auto const &item: ui->tabVariables->selectedItems()) {
        if(item->text(2).size()!=0) {
            Variable var(item->text(0), item->text(1), item->text(2));
            emit addVariable(var);
        }
    }
}
