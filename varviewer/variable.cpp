#include "variable.h"
#include <QDebug>

Variable::Variable()
{
    _value = 0;
    _raw_value = "0";
}

Variable::Variable(const QString &name, const QString &type, const QString &addr) {
    _name = name;
    setType(type);
    setAddress(addr);
}

void Variable::setAddress(const QString &addr) {
    _address = addr;
    _raddr = addr.toInt(nullptr, 16);
}

void Variable::setValue(const QString &value) {
    _raw_value = value;

    if(type()=="float") {
        //char buff[16];
        qint32 x = value.toUInt(nullptr, 16);
        _value =  *((float *)&x);

        return;
    }

    if(isSigned()) {
        QString binary;
        qint64 t = value.toUInt(nullptr, 16)-1;
        binary.setNum(t, 2);
        // odwrócenie bitów
        for(int i=0; i<binary.size(); i++) {
            if(binary[i]=='1') binary[i]='0';
            else binary[i]='1';
        }
        _value = binary.toInt(nullptr, 2);
        _value = -_value;
    }
    else {
        _value = value.toInt(nullptr, 16);
    }
}

void Variable::setType(const QString &type) {
    _type = type;
    if(type == "uint8_t" || type == "uint16_t" || type == "uint32_t") _signed=0;
    else _signed=1;
}

/********************************* VariableVector *********************************/
void VariableVector::addVariable(const Variable &var) {
    mutex.lock();
    append(var);
    mutex.unlock();
}

int VariableVector::findByName(const QString &name) {
    QVector<Variable>::Iterator iter;
    unsigned int index=0;
    mutex.lock();

    for(iter=begin(); iter!=end(); iter++) {
        if(iter->name() == name) {
            mutex.unlock();
            return index;
        }
        ++index;
    }

    mutex.unlock();
    return -1;
}

bool VariableVector::setValueByAddress(const QString &value, const QString &addr) {
    QVector<Variable>::Iterator iter;

    mutex.lock();
    for(iter=begin(); iter!=end(); iter++) {
        if(iter->address() == addr) {
            iter->setValue(value);
            mutex.unlock();
            return 1;
        }
    }

    return 0;
}

Variable VariableVector::at(int i) {
    QMutexLocker locker(&mutex);
    return QVector<Variable>::at(i);
}

QVector<Variable> VariableVector::getVariablesVector() {
    QMutexLocker locker(&mutex);
    return (QVector<Variable>)*this;
}

QVector<double> VariableVector::getValuesVector() {
    QVector<double> values;
    VariableVector::Iterator iter;
    mutex.lock();

    for(iter = begin(); iter != end(); iter++) {
        values.append(iter->value());
    }
    mutex.unlock();

    return values;
}
