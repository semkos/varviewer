#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*!
 * \file
 * \brief Definicja klasy MainWindow
 *
 * Definicja klasy realizującej główne okno programu.
 */

#include <QMainWindow>
#include "importdialog.h"
#include "variable.h"
#include <vector>
#include "barchart.h"
#include "openocd.h"
#include <QTimer>

namespace Ui {
class MainWindow;
}

/*!
 * \brief Główne okno programu
 *
 * Klasa realizuje interfejs graficzny głównego okna programu.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionImport_variables_triggered();
    void importVariable(const Variable &var);
    void refreshMainTab();
    void on_butStart_clicked();
    void openocdConnected();
    void openocdDisconnected();
    void autoScaleChanged(const bool &state);

    void on_edYMax_editingFinished();
    void on_edYMin_editingFinished();
    void on_butYMaxAuto_clicked();

    void refreshGraphs();

signals:
    void valueChanged(const int &index, const qreal &value);

private:
    Ui::MainWindow *ui;
    ImportDialog *importdialog;

    VariableVector varsTracked;
    BarChart *barchart;
    OpenOCD *openocd;
    QTimer *rtimer;

    void loadTrackedVarsToTable();
    void loadMainTab();
};

#endif // MAINWINDOW_H
