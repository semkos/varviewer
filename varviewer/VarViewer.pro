#-------------------------------------------------
#
# Project created by QtCreator 2017-04-13T13:01:24
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VarViewer
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    importdialog.cpp \
    variable.cpp \
    barchart.cpp \
    openocd.cpp

HEADERS  += mainwindow.h \
    importdialog.h \
    variable.h \
    barchart.h \
    openocd.h

FORMS    += mainwindow.ui \
    importdialog.ui

LIBS += -lpthread

CONFIG += c++11 qwt

RESOURCES += \
    media.qrc
