#ifndef OPENOCD_H
#define OPENOCD_H

/*!
 * \file
 * \brief Definicja klasy OpenOCD
 *
 * Definicja klasy reprezentującej proces OpenOCD i realizującej
 * komunikację z mikrokontrolerem.
 */

#include "variable.h"
#include <QProcess>
#include <QTcpSocket>
#include <QTimer>
#include <QVersionNumber>
#include <condition_variable>
#include <mutex>
#include <chrono>

/*!
 * \brief Komunikacja z procesem %OpenOCD
 *
 * Klasa zapewnia komunikację z procesem OpenOCD oraz realizuje odczyt danych
 * z pamięci mikrokontrolera.
 */
class OpenOCD: public QObject
{
    Q_OBJECT

public:
    OpenOCD();
    ~OpenOCD();

    int startTracking();
    void stopTracking();
    bool isConnected();
    void setElfFile(const QString &elf_file) { elfFile = elf_file; }
    void setVariableVector(VariableVector *vect) { vars = vect; }

private slots:
    void updateTracking();
    void socketRead();
    void socketConnected();
    void socketDisconnected();
    void socketError(QAbstractSocket::SocketError err);

signals:
    void connected();
    void disconnected();

private:
    int getVersionNumber();
    int getProcessorFamily();
    int generateConfigFile();
    void showErrorMessage(const QString &mess);
    void handleError(const QString &err_mess);

    QProcess *proc;
    QTcpSocket *socket;
    QTimer *timer;
    unsigned int timer_delay;
    QString config_file;
    QString elfFile;
    QString processorFamily;
    QVersionNumber versionNum;
    QStringList basic_types;
    VariableVector *vars;

    volatile bool wr;
    std::mutex lock;
    std::condition_variable r_ready;
};

#endif // OPENOCD_H
