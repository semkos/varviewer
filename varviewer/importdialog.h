#ifndef IMPORTDIALOG_H
#define IMPORTDIALOG_H

/*!
 * \file
 * \brief Definicja klasy ImportDialog
 *
 * Definicja klasy realizującej okno importowania zmiennych z
 * pliku ELF.
 */

#include <QMainWindow>
#include <vector>
#include "variable.h"
#include <QProcess>
#include <QVector>
#include <QTreeWidgetItemIterator>

namespace Ui {
class ImportDialog;
}

/*!
 * \brief Okno importowania zmiennych
 */
class ImportDialog : public QMainWindow
{
    Q_OBJECT

public:
    explicit ImportDialog(QWidget *parent = 0);
    ~ImportDialog();

    QString const fileName() { return _fileName; }
    void setFileName(const QString & name) { _fileName = name; }

signals:
    void addVariable(const Variable &var);

private slots:
    void on_butSelectFile_clicked();
    void on_butImport_clicked();

private:
    void findVarsAddresses();
    QString getVarAddress(const QString &name);
    QStringList getVarType(const QString &name);
    int insertArray(const QString &arname, QTreeWidgetItemIterator &pos);
    int insertStruct(const QString &name, const QStringList &list, QTreeWidgetItemIterator &pos);
    int completeVarInfo(QTreeWidgetItemIterator &pos);

    Ui::ImportDialog *ui;
    QString _fileName;
    QProcess gdb;
};

#endif // IMPORTDIALOG_H
