QMAKE=qmake

run: all
	build/VarViewer

all: build
	$(QMAKE) -makefile -config release -o build/Makefile varviewer/VarViewer.pro
	make -j4 -C build

build:
	mkdir build

clean:
	rm -f build/*
