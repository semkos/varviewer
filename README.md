# VarViewer - DanielStudio #

Variables monitoring tool for STM32 microcontrollers. Equivalent of STMStudio for Linux.

**Project is in very early stage of development, so expect a lot of bugs :)**

To start using VarViewer you can download statically built application from [Downloads](https://bitbucket.org/semkos/varviewer/downloads/) section or build it from sources by yourself.

### Current functionality ###
* Basic variable types viewing (int, float)
* Simple bar graph

### Screenshots ###
[![s2m.png](https://bitbucket.org/repo/BgggGgX/images/148000710-s2m.png)](https://bitbucket.org/repo/BgggGgX/images/3455621463-s2b.png)
[![s1m.png](https://bitbucket.org/repo/BgggGgX/images/2208774614-s1m.png)](https://bitbucket.org/repo/BgggGgX/images/2560342491-s1b.png)

### Requirements ###
You need following packages to start using VarViewer:

* openocd
* gdb (should be installed by default in most distros)

### Building and installation ###
To build VarViewer from sources you will need:

* qmake (ver>=4)
* Qwt library

To compile, simply execute "make" in project directory
```
#!bash

make
```